// Note: This xsjs file can be invoked by the following url:
// http://<host_ip>:8000/ta/json/server.xsjs
// POST with request header Content-Type: application/json

// Write a message to the server side log found in the Administration Perspective on the Default Admin Window Diagnosis Files sub-tab.
// Look for a file of the form xsengine_alert_lt5013.trc with recent modification date.
// Run the script with trace statements in it and immediately refresh the log window.
// You'll have to search for your entires because it's logging for the whole server.


/* Sample POST request (use in Firefox RESTClient)

{
    "ID": 0,
    "FILE_NAME": "badreview.twitter",
    "LANGUAGE": "en",
    "CONTENT": "Joyce Couturier Denham awards 1 star to KC's Ribs Shack WAS one of our ...! And I now will never recommend this place to anyone!!! I will never go again!!"
}
*/


//$.trace.debug("request path: " + $.request.getPath());
$.trace.info(":TA Server START:");

var debugging = false;  // Turn on debugging to get session messages.

//Create STATUS codes as necessary
var STATUS = { 
		"OK":0, 
		"UNHANDLED":1, 
		"NOT_OBJECT":100, 
		"NOT_ARRAY":101, 
		"HR_ERROR":201,
		"CO_ERROR":202,
		"LAST_STATUS":999
		};

var output = "";

//Check if an object is empty
function is_empty(obj) {

 // Assume if it has a length property with a non-zero value
 // that that property is correct.
 if (obj.length && (obj.length > 0)) {
     return(false);
 }

 if (obj.length && (obj.length === 0)) {
     return(true);
 }

 var key;
 for (key in obj) {
     if (obj.hasOwnProperty(key)) {
        return(false);
     }
 }

 return(true);
}

var gsid = 0;	//Global Session ID
var gconn = {}; //Global DB gconnection
var guid = 0;	//Global User ID
var gdid = 0;	//Global Device ID
var g_xmit_ts = "";	//Global Xmit TS


function insertMSG(sid,uid,did,msg) {
	var sql = "";

	var escapedmsg = String(msg).replace(/'/g,"\\'");	// Weird, had to explicitly cast msg to String for .replace to work
	
	sql = "insert into \"TA\".\"ta.data::msg\" values(\"TA\".\"ta.data::msgId\".NEXTVAL," + sid + "," + uid + "," + did + ",'" + escapedmsg + "',CURRENT_UTCTIMESTAMP)";

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
}

function logMSG(msg) {
    if (debugging) {
        insertMSG(gsid,guid,gdid,msg);
    }
}


function insertReview(id,fn,lang,cont) {
	var sql = "";

	var escapedcont = String(cont).replace(/'/g,"''");	// Weird, had to explicitly cast cont to String for .replace to work

	//sql = "insert into \"OPENSAP_TA_WORKSHOP\".\"student00.data::PRODUCT_REVIEWS\" values(" + sid + ",'" + fn + "','" + lang + "','" + escapedcont + "')";
	sql = "insert into \"mta_ta.db::data.Messages\" values(\"mta_ta.db::msgId\".NEXTVAL,'" + escapedcont + "',CURRENT_UTCTIMESTAMP)";

	//logMSG("insertNOTE SQL: " + String(sql).replace(/'/g,"\\'"));
	//logMSG("insertNOTE SQL: " + sql);
	$.trace.info(":TA Server insertReview SQL:" + sql + ":");

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
	return(result);
	
}

//Define needed functions to process various types of data
function processReviewDATA(data) {
	
	// Expecting data to be of the following form:
/*	
	{
	    "ID":0,
	    "FILE_NAME":"badreview.twitter",
	    "LANGUAGE":"en",
	    "CONTENT":"Joyce Couturier Denham awards ...  to anyone!!! I will never go again!!"
	}
*/

	logMSG("Processing Review Data");

	var result = {};
	var msg = "";
	
	if (data instanceof Object) {
		var review_data = data;
		
		if (review_data instanceof Object) {
			// logMSG("review_data is an Object as expected.");
			var id = review_data.ID;
			var fn = review_data.FILE_NAME;
			var lang = review_data.LANGUAGE;
			var cont = review_data.CONTENT;
			msg += "  review_data =  LANG: " + lang + " CONTENT: " + cont.substring(0, 20) + "...\n";
			
			// Save the reveiw into HANA
			
			var reviewResult = insertReview(id,fn,lang,cont);

			var reviewID = 0;
			
			result.status = STATUS.OK;
			result.msg = "OK reviewID: " + reviewID + " " + msg;
			result.reviewID = reviewID;
			logMSG("review_data processed OK returning reviewID: " + reviewID + ".");
			gdid = 0;
			
			
			// From this doc
			// http://help.sap.com/saphelp_hanaplatform/helpdata/en/25/0cdcaf0c2a44a6905586a0dbc7fea9/content.htm?frameset=/en/8f/5020a8e19d40a781aabeefa204c2ae/frameset.htm&current_toc=/en/8f/5020a8e19d40a781aabeefa204c2ae/plain.htm&node_id=19
			// Text Analysis XS API Example Application
			
			if (false) {
			// Create a text analysis session that uses an out-of-the-box configuration EXTRACTION_CORE
            //var oTextAnalysisSession  = new $.text.analysis.Session({configuration:'EXTRACTION_CORE'});
            var oTextAnalysisSession  = new $.text.analysis.Session({configuration:'EXTRACTION_CORE_VOICEOFCUSTOMER'});

            // Input text to be analyzed
            var sText = "New York, New York, this city's a dream";
            //sText = cont;

            // Call the analyze method. Explicitly set the language, although the default is English anyway
            var oAnalysisResult = oTextAnalysisSession.analyze({inputDocumentText: sText, language: 'en'});

            result.ta = oAnalysisResult;
            
            // Send the results back
            //$.response.contentType = 'text/json';
            //$.response.setBody(JSON.stringify(oAnalysisResult));


            // Text Mining XS API Example Application
            // http://help.sap.com/saphelp_hanaplatform/helpdata/en/11/f8f9da30c34c63ba0f56995a39a39c/content.htm?frameset=/en/ff/0e249e196b454ea3734a28c412ab28/frameset.htm&current_toc=/en/ff/0e249e196b454ea3734a28c412ab28/plain.htm&node_id=21
			}
		}
		else if (review_data instanceof Array) {
			logMSG("review_data is an Array");
			result.status = STATUS.HR_ERROR;
			result.msg = "Error:" + " Expected an object not an array.";
		}
		else {
			logMSG("procReview: Not a handled data object.");
			result.status = STATUS.UNHANDLED;
			result.msg = "Not a handled data object";
		}
	}
	else {
		logMSG("procReview: expecting object at top level.");
	}
	return(result);
}


//Doesn't work with JSON.parse (object member names must be double quoted.)
//var strJSON = '{cmd:"HRDATA",data:[{chr:78},{chr:79},{chr:82}]}';

//Works with JSON.parse
var strJSON = '{"cmd":"HRDATA","data":[{"chr":78},{"chr":79},{"chr":82}]}';


try {
	// Always assume and exception can occur

	var i=0;
	
	//var request_method = $.request.getMethod();	// Deprecated syntax doesn't work!?
	var request_method = $.request.method;
	var method = "";
	
	method += $.request.toString();

	switch(request_method) {
	case $.net.http.GET:	// GET == 1
		method = "GET";
		//var jsonParam = $.request.getParameter("json"); // Deprecated syntax doesn't work!
		strJSON = "";
		// Parameters from the HTTP GET query string
		for (i=0; i<$.request.parameters.length; ++i) {
		    var name = $.request.parameters[i].name;
		    var value = $.request.parameters[i].value;
		    if (name === "json") {
				strJSON = value;
				// method += "json: " + value;
		    }
		    else {
				method += " only param named json accepted. not " + name;
		    }
		}
		if (strJSON !== "") {
			method += " " + strJSON;
		}
		else {
			method += " empty param named json.";
		}
		break;
	case 2: // Unknown
		method = "Unknown";
		break;
	case $.net.http.POST: // POST = 3
		method = "POST";
		
		// Loop through all the HTTP headers and only process the body if content-type is application/json
		var content_type = "";
		for (i=0; i<$.request.headers.length; ++i) {
		    var name = $.request.headers[i].name;
		    var value = $.request.headers[i].value;

		    //method += " header[" + i + "] = " + name + ":" + value + "\n";

		    if (name === "content-type") {
				content_type = value;
			    method += " header[" + i + "] = " + name + ":" + value + "\n";
		    }
 
		}
		if (content_type.indexOf("application/json") !== -1) {
			// Extract the body of the POST
			strJSON = $.request.body.asString();
			//method += " POSTED BODY: " + strJSON;
		}
		else {
			method += "\n ERROR: expected content-type:applicaton/json.";
		}
		
		break;
	case $.net.http.PUT: // PUT = 4
		method = "PUT";
		break;
	case $.net.http.DEL: // DELETE = 5
		method = "DELETE";
		break;
	case 0: // OPTIONS
		method = "OPTIONS";
		break;
	case $.net.http.gconnECT: // gconnECT = 7
		method = "gconnECT";
		break;
	default:
		method = "Unhandled";
		break;
	}

	output = "TA JSON Server : " + method + "\n\n";

	gconn = $.db.getConnection();    // User these credentials even if authenticated under a different user (or anonymous)
	
	// Parse the JSON string into an object
	var objJSON = JSON.parse(strJSON);

	//alert("JSON: \n\n" + strJSON);

	output += "strJSON = " + strJSON + "\n";

	//output += "objJSON = " + objJSON + "\n";
	
	output += "\n";	

	var propt;
	var objResult = {};

	objResult.status = STATUS.OK;

	if (objJSON instanceof Object) {
		output += "OK:" + " Do processReviewDATA.";
		objResult = processReviewDATA(objJSON);
		logMSG("OK:" + " Do processReviewDATA.");

	}
	else {
		// Expected top level JSON element to be an object(containing an array of cmd objects)
		output += "Error:" + " Expected an object at the top level.";
	}
	

	output += "\nFINISHED... \n";
	
	if (!(is_empty(objResult))) {
	    $.response.contentType = "application/json";
        $.response.setBody(JSON.stringify(objResult));
	}
	else {
		$.response.contentType = "text/html";
		$.response.setBody(output);
	}
	
	logMSG("Session ========== " + gsid + " ========== END.");
	
} catch (exception) {
	// Figure out what kind of exception it is and handle ones we care about.
	if (exception instanceof TypeError) {
		// Handle TypeError exception
		output += "TypeError: " + exception.toString() + "\n";
	} else if (exception instanceof ReferenceError) {
		// Handle ReferenceError
		output += "ReferenceError: " + exception.toString() + "\n";
	} else if (exception instanceof SyntaxError) {
		// Handle SyntaxError
		output += "SyntaxError: " + exception.toString() + "\n";
	} else {
		// Handle all other not handled exceptions
		output += "ExceptionError: " + exception.toString() + "\n";
	}
	
	$.response.contentType = "text/html";
	$.response.setBody(output);
	
} finally {
	// This code is always executed even if an exception is 
	if (gconn instanceof Object) {
		gconn.close();
	}
}

$.trace.info(":TA Server END:");


